output "nginx_dns_lb" {
  description = "DNS load balancer"
  value = aws_lb.main-load-balancer.dns_name
}
