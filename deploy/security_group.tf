resource "aws_security_group" "alb" {
  name = "alb-sg"
  vpc_id = aws_vpc.main.id
  ingress {
    protocol = "tcp"
    from_port = "80"
    to_port = "80"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "alb-sg"
  }
}

resource "aws_security_group" "ecs_task" {
  name = "ecs-task-sg"
  vpc_id = aws_vpc.main.id
  ingress {
    protocol = "tcp"
    from_port = var.port
    to_port = var.port
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "ecs-task-sg"
  }
}