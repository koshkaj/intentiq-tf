resource "aws_lb" "main-load-balancer" {
  name               = "main-load-balancer"
  internal           = false
  load_balancer_type = "application"
  subnets            = [aws_subnet.public-a.id, aws_subnet.public-b.id, aws_subnet.public-c.id]
  security_groups    = [aws_security_group.alb.id]

  tags = {
    Name = "main-load-balancer"
  }
}

resource "aws_lb_target_group" "main-tg" {
  name        = "main-tg"
  port        = var.port
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"
  health_check {
    healthy_threshold   = "5"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "3"
  }
  tags = {
    Name = "main-tg"
  }
}

resource "aws_lb_listener" "nginx" {
  load_balancer_arn = aws_lb.main-load-balancer.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_lb_target_group.main-tg.arn
    type             = "forward"
  }
}