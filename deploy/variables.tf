variable "region" {
    type = string
    default = "eu-north-1"
}
variable "port" {
    type = number
    default = 80
}

variable "container_name" {
    type = string
    default = "nginx-con"
}

variable "container_cpu_request" {
    type = number
    default = 256
}

variable "container_memory_request" {
    type = number
    default = 512
}

variable "platform" {
    type = string
    default = "FARGATE"
}

variable "image" {
    type = string
    default = "nginx:latest"
}

variable "instance_count" {
    type = number
    default = 1
}
