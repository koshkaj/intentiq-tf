resource "aws_ecs_cluster" "nginx_cluster" {
  name = "nginx_cluster"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}
resource "aws_ecs_task_definition" "nginx_task" {
  family                   = "nginx_task"
  requires_compatibilities = ["FARGATE", "EC2"]
  network_mode             = "awsvpc"
  cpu                      = var.container_cpu_request
  memory                   = var.container_memory_request
  container_definitions = jsonencode([
    {
      name      = var.container_name
      image     = var.image
      essential = true
      portMappings = [
        {
          protocol      = "tcp"
          containerPort = var.port
          hostPort      = var.port
        }
      ]
    }
  ])
}


resource "aws_ecs_service" "nginx_service" {
  name            = "nginx_service"
  cluster         = aws_ecs_cluster.nginx_cluster.id
  task_definition = aws_ecs_task_definition.nginx_task.arn
  desired_count   = var.instance_count
  launch_type     = var.platform
  network_configuration {
    subnets         = [aws_subnet.public-a.id, aws_subnet.public-b.id, aws_subnet.public-c.id]
    security_groups = [aws_security_group.ecs_task.id]
    assign_public_ip = true
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.main-tg.arn
    container_name   = var.container_name
    container_port   = var.port
  }
  tags = {
    Name = "nginx_service"
  }

}

