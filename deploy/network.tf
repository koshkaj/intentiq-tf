resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags = {
    Name = "main-vpc"
  }
}

resource "aws_subnet" "public-a" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "${var.region}a"
  map_public_ip_on_launch = true
  tags = {
    Name = "ecs-nginx-public_1"
  }
}

resource "aws_subnet" "public-b" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "${var.region}b"
  map_public_ip_on_launch = true
  tags = {
    Name = "ecs-nginx-public_2"
  }
}

resource "aws_subnet" "public-c" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.3.0/24"
  availability_zone = "${var.region}c"
  map_public_ip_on_launch = true
  tags = {
    Name = "ecs-nginx-public_3"
  }
}

resource "aws_internet_gateway" "main-vpc-ig" {
  vpc_id = aws_vpc.main.id
}

resource "aws_route_table" "main-vpc-ig-rt" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-vpc-ig.id
  }
}

resource "aws_main_route_table_association" "aws-route-table-association" {
  vpc_id = aws_vpc.main.id
  route_table_id = aws_route_table.main-vpc-ig-rt.id
}