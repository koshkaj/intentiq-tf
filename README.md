## Description
This terraform project spins up a VPC with 3 different **Public** subnets in three availability zones in `eu-north-1` region.
After that It creates Application Load Balancer with Elastic Container Service that runs an nginx task underneath it.
Both ALB and ECS have security groups defined to control incoming  and outgoing traffic.

After `terraform apply` it returns the ALB dns_name that forwards the request to the ECS service.


## How to run
**NOTE**: in `main.tf` file variables `access_key` and `secret_key` should be specified for AWS environment

go to `./deploy` and run the following terraform commands

```shell
terraform init

terraform plan

terraform apply
```


